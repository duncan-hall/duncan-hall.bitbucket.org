
var tve = angular.module('ax-tve', ['ngRoute']);

/********************************************************
 * TEMPLATE ROUTER
 ********************************************************/
tve.config(function ($routeProvider) {

	$routeProvider
	.when('/', {
		controller: 'MainCtrl',
		templateUrl: 'app/partials/home.html'
	})
	.when('/item', {
		controller: 'MainCtrl',
		templateUrl: 'app/partials/item.html'
	})		
	.otherwise({
		redirectTo: '/'
	});
});


/********************************************************
 * BASE CONTROLLER FOR ALL PARTIAL VIEWS
 ********************************************************/
tve.controller('MainCtrl', function ($scope, AppLoader) {

	$scope.$on('$viewContentLoaded', function () {

		AppLoader.isLoading = false;
		$scope.isLoading = false;
		$scope.$watch(function () { return AppLoader.isLoading; }, function (newValue) {
			$scope.isLoading = newValue;
		});

		/**
		* This is a little spurious but required to ensure picturefill images
		* get rendered after a partial is loaded (and we aren't *directly* touching the DOM)
		*/		
		window.picturefill();
	});
});


/********************************************************
 * LOADING SPINNER CONTROLLER 
 ********************************************************/
tve.controller('LoadingCtrl', function ($scope, AppLoader) {

	$scope.isLoading = AppLoader.isLoading;
	$scope.$watch(function () { return AppLoader.isLoading; }, function (newValue) {
		$scope.isLoading = newValue;
	});
});


/********************************************************
 * CHANGE APP STATE WHEN LOCATION CHANGES
 ********************************************************/
tve.run(function ($rootScope, $location, AppLoader) {

	$rootScope.$on("$locationChangeSuccess", function(event, next, current){
		AppLoader.isLoading = true;
	});
});

/********************************************************
 * APPLICATION STATE FACTORY
 ********************************************************/
tve.factory('AppLoader', function () { return {isLoading:true}; });